# London Tube Visualizer

__Membres de l'équipe__ : Brede Swann, Colomberotto Teo et Zerika Karim

Le réseau actuel compte 12 lignes et dessert 280 stations (dont certaines fermées) sur près de 402 kilomètres. Il permet le transport de 1,379 milliard de voyageurs lors du cycle annuel 2016-2017, soit une moyenne quotidienne de plus de 4,8 millions de passagers. Ce projet a pour but de visualiser pour chaque arrêt du métro les points de locations de vélo les plus proches. Il permettra de sortir certaines statistiques sur la quantité de vélo disponible et le temps de marche pour y accèder.

## Choix des données

Comme le métro de Londres est l'un des plus connu dans le monde. Plein de données sont disponibles. Il est possible de trouber tout et n'importe quoi dans ce domaine. Le site web: https://tfl.gov.uk/info-for/open-data-users/ regorge de pleins de différentes données et d'API pour récolter en direct différentes données. Donc notre cas, nous utilisons une API afin de récolter les données de la quantité et de la position des vélos en direct. 

Afin de nous permettres de travailler correctement sur le projet et d'avoir toutes les possibilitées que nous voulons. Nous travaillons avec une carte .SVG des métros de Londres. Le lien: https://misc.oomap.co.uk/misc/tubemap/ . Cela nous a permis de séparer les différentes lignes. Et de les retravailler comme nous le voulions. 

## Technologies utilisées

Ce projet est réalisé en tant qu'application Web, avec une partie frontend pour la visualisation de l'information et une partie backend pour la gestion des données. Pour la partie backend, nous n'avons rien fait. Nous avons pu directement utiliser l'API fournit par le site du métro de Londres. Les technologies utilisées sont les suivantes :

* __Frontend__ :
    * __viteJS__ :
    * __vueJS__ :
    * __D3__ :
    * __OpenStreetMap__ :

* __Backend__ :
    * __https://tfl.gov.uk/info-for/open-data-users__ 

## Intention et public cible

L'intention de notre projet est de créer une application WEB pour visualiser la carte des métros de Londres de manière interactive, afin de faciliter la navigation et la planification de déplacements pour les utilisateurs de la ville. En plus de montrer l'emplacement des stations de métro, notre application affichera également la quantité de vélos louables disponibles dans chaque station, permettant aux utilisateurs de prendre en compte cette option de transport dans leurs déplacements.

Le public cible de notre application est principalement constitué de personnes habitant ou visitant Londres et souhaitant se déplacer de manière pratique et écologique dans la ville. Notre application s'adresse également aux personnes souhaitant avoir un aperçu en temps réel de la disponibilité des vélos en location dans les différentes stations de la ville, afin de planifier au mieux leurs déplacements.

## Exécution

Le lien de l'application WEB est: https://londontubevisualiser.web.app/

Lors du lancement de l'application, deux possibilités sont offert à l'utilisateur.

<img src="images/image-8.png" width="400"/>

L'explication des deux choix est présente lorsque l'on passe la souris sur le logo (i). 

Pour résumer les choix.
- Realtime Data, va venir télécharger les dernières données à jour sur l'API. Cela peut facilement prendre 1-2 minutes suivant la connexion. Il y a beaucoups de données.

- Sample Data, va venir charger un set de données déjà en place sur l'application. Cela veut dire que les données ne seront pas en temps réel mais au moins il y a l'avantage de pouvoir directement utiliser l'application. 

## Représentation

### Ligne de métro

![image.png](images/image.png)

En évidence sont présenté les lignes de métro fonctionnelles avec l'application. Il s'agit du carte de métro. Donc les distances ne sont pas proportionnelles à la réalité, elles est fait afin d'être pratique pour les utilisateurs. Les noms des lignes sont écris sur la partie haute droite. Les couleurs correspondent au bonne ligne. 

![image-1.png](images/image-1.png)

Lorsque l'on pointe une cercle qui représente un arrêt, celui-ci met sa ligne en évidence (son nom aussi) et affiche le nom de l'arrêt. La quantité de vélos proche de cette station est alors affiché. 

L'affichage de la rivière Tamises est là pour aider à la visualisation de la position de la carte dans Londres. 

### Visualisation précises 

Lorsque l'on clique avec la souris de l'ordinateur sur une station, une nouvelle fenêtre s'ouvre. 

![image-2.png](images/image-2.png)

Cette fenêtre présente une carte OpenStreetMap dans laquelle il est possible de ce déplacer. Il y est est affiché les points précis d'ou se trouvent les vélos. Sur la partie droite, il y a des graphiques en camambert qui représenten la disponibilité des docks pour les vélos ainsi que les types de vélos (électrique ou non). Il est possible de pointer avec la souris une station précise afin de d'obtenir les statistique de celles-ci. 

Il est possible de changer le mode de visualisation en passant en visualisation globale. Il suffit d'appuyer sur "Show Overall Info". 

<img src="images/image-3.png" width="500"/>

Cela affiche de histogrammes de tous les points proches de la station. Ils montrent la disponibilité des docks ainsi que les types de vélos disponibles. 

### Police d'écriture

La police d'écriture pour l'application a été choisi pour être simple et facilement lisible par le plus grand nombre de personnes. Les différentes polices sont: 

* __Polices__ :
    * __Avenir__ :
    * __Helvetica__ :
    * __Arial__ :

Elles sont principalement noir sur fond blanc et dans la fenêtre qui affiche la map, blanc sur fond noir.

### Couleurs

Les couleurs des lignes de métros sont celles fournis de base avec la carte SVG. Ces couleurs sont facilement distinguables et sont les couleurs officiels de la carte de métro de Londres.

Concernant les graphiques, les deux couleurs sont: 

* __Vert__ :
    * __#7FC97F__ :

Elle est souvent utilisée dans les designs de sites Web, les graphiques, et d'autres créations pour ajouter de la luminosité et de la vitalité.

* __Violet__ :
    * __#BEAED4__ :

Cette couleur est souvent utilisée dans les designs de sites Web et de graphiques pour créer une ambiance apaisante et élégante.

Les couleurs #BEAED4 et #7FC97F sont toutes deux visibles pour les daltoniens car elles ne comprennent pas uniquement du rouge, du vert et du bleu, mais plutôt une combinaison de ces trois couleurs. Cela les rend plus faciles à distinguer pour les personnes atteintes de daltonisme. 

## Interaction

Il est possible de venir modifier la taille des cercles qui entoure les stations pour aider la visualisation si besoins. La différence de tailles entre chaque point des stations représente la différence de quantité de vélos disponible. 

<img src="images/image-4.png" width="400"/>
<img src="images/image-5.png" width="400"/>

Il est bien sur possible de zoomer et de se déplacer grâce à la souris ou au trackpad de l'ordinateur. Mais il y a aussi la possibilité de le faire grâce à l'option en bas à gauche de l'écran.

<img src="images/image-6.png" width="100"/>

Dans la fenêtre d'intéraction pour les stations, il est aussi possible de revenir en arrière si besoins avec "Back to the tube map". 

![image-7.png](images/image-7.png)

